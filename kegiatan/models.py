from django.db import models
from datetime import date,datetime
from django.utils import timezone

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(max_length = 100)
    start_time = models.CharField(max_length = 100)
    end_time = models.CharField(max_length = 100)
    description = models.TextField(blank=True, null=True)