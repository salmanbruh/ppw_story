from django import forms
from .models import Jadwal
from bootstrap_datepicker_plus import DateTimePickerInput

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = [
            'kategori',
            'name',
            'place',
            'start_time',
            'end_time',
        ]

        labels = {
            'kategori':'Kategori',
            'name':'Kegiatan',
            'place':'Tempat',
            'start_time':'Waktu mulai',
            'end_time':'Waktu berakhir',
        }

        widgets = {
            'kategori': forms.Select(
                attrs={
                    'class': 'form-control',
                },
            ),
            'name':forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Masukkan nama kegiatan anda',
                },
            ),
            'place': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Masukkan tempat kegiatan anda',
                },
            ),
            'start_time':DateTimePickerInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'yyyy/mm/dd HH:MM'
                },
            ),
            'end_time':DateTimePickerInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'yyyy/mm/dd HH:MM'
                },
            ),
        }
        