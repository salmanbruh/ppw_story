from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'intothevoid/index.html');

def resume(request):
    return render(request, 'intothevoid/resume.html');

def contact(request):
    return render(request, 'intothevoid/contact.html');

def kegiatan(request):
    return render(request, 'kegiatan/list_kegiatan.html')